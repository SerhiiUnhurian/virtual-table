#include <iostream>

// TODO: Implement vtable support for all three classes without using of virtual keyword.
// Do not change main() and, if possible, do not change class LevelTwo and class SideLevel.
// Hint: you can add some code to global scope!

class LevelOne {
private:
    int y = 1;

    void simpleImpl() {
        std::cout << "LevelOne: simple." << std::endl;
    }

    int coolImpl(int x) {
        std::cout << "LevelOne: cool!" << std::endl;
        return x+y;
    }

protected:
    struct Vtable {
        void (LevelOne::*vSimple)() {nullptr};
        int (LevelOne::*vCool)(int) {nullptr};
    } vTable;

public:
    LevelOne() {
        vTable.vSimple = (void(LevelOne::*)()) &LevelOne::simpleImpl;
        vTable.vCool = (int(LevelOne::*)(int)) &LevelOne::coolImpl;
    };

    void simple() {
        return (this->*(vTable.vSimple))();
    }

    int cool(int x) {
        return (this->*(vTable.vCool))(x);
    }
};

class LevelTwo : public LevelOne {
private:
    int y = 2;

public:
    LevelTwo() {
        vTable.vSimple = (void(LevelOne::*)()) &LevelTwo::simple;
        vTable.vCool = (int(LevelOne::*)(int)) &LevelTwo::cool;
    }

    void simple() {
        std::cout << "LevelTwo: very simple." << std::endl;
    }

    int cool(int x) {
        std::cout << "LevelTwo: very cool!" << std::endl;
        return x+y;
    }
};

class SideLevel : public LevelOne {
private:
    int y = 3;

public:
    SideLevel() {
        vTable.vSimple = (void(LevelOne::*)()) &SideLevel::simple;
        vTable.vCool = (int(LevelOne::*)(int)) &SideLevel::cool;
    }

    void simple() {
        std::cout << "SideLevel: quite simple." << std::endl;
    }

    int cool(int x) {
        std::cout << "SideLevel: quite cool!" << std::endl;
        return x+y;
    }
};

int main() {

    // Expected: LevelOne function calls
    LevelOne o;
    o.simple();
    o.cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelTwo t;
    t.simple();
    t.cool(5);

    SideLevel s;

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne &ot = t;
    ot.simple();
    ot.cool(5);

    std::cout << std::endl;

    // Expected: SideLevel function calls
    LevelOne &os = s;
    os.simple();
    os.cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne *otp = &t;
    otp->simple();
    otp->cool(5);

    std::cout << std::endl;

    // Expected: LevelTwo function calls
    LevelOne *ptr = new LevelTwo();
    ptr->simple();
    ptr->cool(5);

    return 0;
}