## Task:

Implement vtable support for all three classes without using of `virtual` keyword.  
Do not change `main()` and, if possible, do not change class `LevelTwo` and class `SideLevel`.  

**Hint:** you can add some code to global scope!